/* Copyright (C) 2019 Sam Bazley
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "colortool.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX(a, b) (a>b?a:b)
#define MIN(a, b) (a>b?b:a)
#define CLAMP(x, a, b) (x < a ? a : (x > b ? b : x))

enum Property {
    R,
    G,
    B,
    H,
    S,
    V
};

enum Operation {
    SET,
    ADD,
    REL
};

static int ranges [] = {
    255, //R
    255, //G
    255, //B
    60,  //H (360 / 6)
    100, //S
    100  //V
};

struct HSV rgb2hsv(struct RGB rgb) {
    struct HSV hsv;

    float min = MIN(MIN(rgb.r, rgb.g), rgb.b);
    float max = MAX(MAX(rgb.r, rgb.g), rgb.b);
    float delta = max - min;

    hsv.v = max;

    if (delta == 0) {
        hsv.h = 0;
        hsv.s = 0;
        return hsv;
    }

    if (max == rgb.r) {
        hsv.h = (rgb.g - rgb.b) / delta;
    } else if (max == rgb.g) {
        hsv.h = (rgb.b - rgb.r) / delta + 2;
    } else {
        hsv.h = (rgb.r - rgb.g) / delta + 4;
    }

    if (hsv.h < 0) {
        hsv.h = fmodf(hsv.h, 6) + 6;
    }

    if (hsv.v == 0) {
        hsv.s = 0;
    } else {
        hsv.s = delta / hsv.v;
    }


    return hsv;
}

struct RGB hsv2rgb(struct HSV hsv) {
    struct RGB rgb;

    if (hsv.h == -1) {
        rgb.r = hsv.v;
        rgb.g = hsv.v;
        rgb.b = hsv.v;
        return rgb;
    }

    float a = hsv.v * (1 - hsv.s);
    float b = hsv.v * (1 - (hsv.h - floorf(hsv.h)) * hsv.s);
    float c = hsv.v * (1 - (1 - (hsv.h - floorf(hsv.h))) * hsv.s);

    int col = floor(hsv.h);

    switch (col) {
    case 0:
        rgb.r = hsv.v;
        rgb.g = c;
        rgb.b = a;
        break;
    case 1:
        rgb.r = b;
        rgb.g = hsv.v;
        rgb.b = a;
        break;
    case 2:
        rgb.r = a;
        rgb.g = hsv.v;
        rgb.b = c;
        break;
    case 3:
        rgb.r = a;
        rgb.g = b;
        rgb.b = hsv.v;
        break;
    case 4:
        rgb.r = c;
        rgb.g = a;
        rgb.b = hsv.v;
        break;
    case 5:
        rgb.r = hsv.v;
        rgb.g = a;
        rgb.b = b;
        break;
    }

    return rgb;
}

struct RGB parseRGBString(const char *str) {
    struct RGB rgb;

    if (!str || *str == '\0') {
        goto error;
    }

    if (*str == '#') {
        str++;
    }

    char *end = 0;
    int c = strtol(str, &end, 16);

    if (end && *end) {
        goto error;
    }

    if (strlen(str) == 3) {
        rgb.r = ((c & 0xF00) >> 8) | ((c & 0xF00) >> 4);
        rgb.g = ((c & 0x0F0) >> 4) | ((c & 0x0F0) >> 0);
        rgb.b = ((c & 0x00F) >> 0) | ((c & 0x00F) << 4);
    } else if (strlen(str) == 6) {
        rgb.r = (c & 0xFF0000) >> 16;
        rgb.g = (c & 0x00FF00) >> 8;
        rgb.b = (c & 0x0000FF);
    } else {
        goto error;
    }

    rgb.r /= 255.f;
    rgb.g /= 255.f;
    rgb.b /= 255.f;

    return rgb;
error:
    rgb = (struct RGB) {-1, -1, -1};
    return rgb;
}

static int isValidRGB(struct RGB rgb) {
    if (rgb.r >= 0.f && rgb.r <= 1.f &&
        rgb.g >= 0.f && rgb.g <= 1.f &&
        rgb.b >= 0.f && rgb.b <= 1.f) {
        return 1;
    } else {
        return 0;
    }
}

static enum Property parseProperty(char *str) {
    if (strlen(str) != 1) {
        goto error;
    }

    switch (*str) {
    case 'r':
        return R;
        break;
    case 'g':
        return G;
        break;
    case 'b':
        return B;
        break;
    case 'h':
        return H;
        break;
    case 's':
        return S;
        break;
    case 'v':
        return V;
        break;
    default:
        goto error;
        break;
    }

error:
    return -1;
}

static float parseChange(char *str, enum Operation *op, enum Property prop, int *err) {
    char *cend;
    float change = strtof(str, &cend);

    if (cend == str) {
        *err = 1;
    }

    if (*cend == '%' && *(cend + 1) == '\0') {
        change /= 100.f;
        *op = REL;

        if (*str != '+' && *str != '-') {
            *err = 1;
            return 0;
        }
    } else if (*cend != '\0') {
        *err = 1;
        return 0;
    } else {
        change /= (float) ranges[prop];

        if (*str == '+' || *str == '-') {
            *op = ADD;
        } else {
            *op = SET;
        }
    }

    return change;
}

static void applyChange(float change, float *v, enum Operation op) {
    if (op == REL) {
        *v *= 1.f + change;
    } else if (op == ADD) {
        *v += change;
    } else if (op == SET) {
        *v = change;
    }
}

void validateHSV(struct HSV *hsv) {
    if (hsv->h < 0) {
        hsv->h = fmodf(hsv->h, 6.f) + 6.f;
    }

    hsv->h = fmodf(hsv->h, 6.f);

    hsv->s = CLAMP(hsv->s, 0, 1.f);
    hsv->v = CLAMP(hsv->v, 0, 1.f);
}

void validateRGB(struct RGB *_rgb) {
    float *rgb = (float *) _rgb;
    for (int i = 0; i < 3; i++) {
        rgb[i] = CLAMP(rgb[i], 0, 1.f);
    }
}

int main(int argc, char **argv) {
    if (argc != 3 && argc != 4) {
        fprintf(stderr, "Usage: %s <color> <r|g|b|h|s|v> [[+|-]<n>[%%]]\n", argv[0]);
        return 1;
    }

    struct RGB rgb = parseRGBString(argv[1]);
    if (!isValidRGB(rgb)) {
        fprintf(stderr, "Invalid color\n");
        return 1;
    }

    enum Property prop = parseProperty(argv[2]);
    if (prop == -1) {
        fprintf(stderr, "Invalid property, expecting r, g, b, h, s, or v\n");
        return 1;
    }

    if (argc == 3) {
        struct HSV hsv = rgb2hsv(rgb);

        switch (prop) {
        case H:
        case S:
        case V:
            printf("%d\n", (int) (ranges[prop] * ((float *) &hsv)[prop - H]));
            break;
        case R:
        case G:
        case B:
            printf("%d\n", (int) (ranges[prop] * ((float *) &rgb)[prop]));
            break;
        }
    } else if (argc == 4) {
        enum Operation op;
        int changeErr = 0;
        float change = parseChange(argv[3], &op, prop, &changeErr);
        if (changeErr) {
            fprintf(stderr, "Invalid number\n");
            return 1;
        }

        if (prop == H || prop == S || prop == V) {
            struct HSV hsv = rgb2hsv(rgb);
            applyChange(change, &((float *) &hsv)[prop - H], op);
            validateHSV(&hsv);
            rgb = hsv2rgb(hsv);
        } else {
            applyChange(change, &((float *) &rgb)[prop], op);
            validateRGB(&rgb);
        }

        printf("#%02x%02x%02x\n",
                (int) (rgb.r * 255.f),
                (int) (rgb.g * 255.f),
                (int) (rgb.b * 255.f));
    }

    return 0;
}
