# colortool

Tool for modifying colors.

## Usage

    colortool <color> <r|g|b|h|s|v> [[+|-]<n>[%]]

The \<color\> option must take the format of either rgb or rrggbb with an
optional # prefix.

The \<n\> option can have an optional + or - prefix and an optional % suffix.
If the + or - prefix is present and the % suffix is not, n will be added or
subtracted to the current value. However, if the % suffix is present, the
value will be increased/decreased by n%. When using the % suffix, either
the + or the - prefix must also be used. If no prefix or suffix is present, the
value will be set to n.

## Examples

`colortool abcdef g 10` - Set the green channel to 10.

`colortool f00 b +128` - Add 128 to the blue channel.

`colortool "#ffffff" r -100%` - Set the red channel to 0.

`colortool "$color0" s +50%` - Increase the HSV saturation by 50% of its current value.

`colortool "$color1" s 50` - Set the HSV saturation to 50%.

`colortool "#a0a" h +180` - Add 180° to the hue.
