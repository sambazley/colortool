CFLAGS+=-Iinclude
LDFLAGS+=-lm

DESTDIR?=
PREFIX?=/usr/local
BINDIR?=$(DESTDIR)$(PREFIX)/bin
INCDIR?=$(DESTDIR)$(PREFIX)/include
LIBDIR?=$(DESTDIR)$(PREFIX)/lib

.PHONY: all install install-h install-so uninstall uninstall-h uninstall-so clean

all: colortool

colortool: src/main.o
	$(CC) -o $@ $? $(CFLAGS) $(LDFLAGS)

libcolortool.so: CFLAGS+=-fPIC

libcolortool.so: src/main.o
	$(CC) -o $@ $? $(CFLAGS) $(LDFLAGS) -shared

install: colortool
	mkdir -p "$(BINDIR)"
	cp -fp colortool "$(BINDIR)"

install-h:
	mkdir -p "$(INCDIR)"
	cp -fp include/colortool.h "$(INCDIR)"

install-so: libcolortool.so
	mkdir -p "$(LIBDIR)"
	cp -fp libcolortool.so "$(LIBDIR)"

uninstall:
	rm -f "$(BINDIR)/colortool"

uninstall-h:
	rm -f "$(INCDIR)/colortool.h"

uninstall-so:
	rm -f "$(LIBDIR)/libcolortool.so"

clean:
	rm -f src/*.o colortool libcolortool.so
